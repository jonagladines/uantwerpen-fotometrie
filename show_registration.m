function show_registration(lights_r, filename)
% takes a series of images and creates an animated gif AND PLAYS IT BACK
% Written by Jona Gladines
% For Uantwerpen
% Date June - 2019
% Update April - 2022

    [~,~,noi] = size(lights_r);
    [ySize, xSize] = size(lights_r(:,:, 1));
    fprintf('Rendering registration view:   [  0%]');
    
    fprintf('\b\b\b\b\b\b[%3d%%]',round(100/noi));
    img = lights_r(:,:,1);
    imwrite(uint8(imadjust(uint16(img), [(mean(nonzeros(img))-4*std(nonzeros(img)))/65535, (mean(nonzeros(img))+8*std(nonzeros(img)))/65535])./256), filename, 'gif', 'Loopcount', inf,'DelayTime',0.1)  %apply gamma adjustment for visibility and rescale to uint8 for gif
    for i = 2:noi
        fprintf('\b\b\b\b\b\b[%3d%%]',round((i*100)/noi));
        img = lights_r(:,:,i);
        imwrite(uint8(imadjust(uint16(img), [(mean(nonzeros(img))-4*std(nonzeros(img)))/65535, (mean(nonzeros(img))+8*std(nonzeros(img)))/65535])./256), filename, 'gif','WriteMode','append', 'DelayTime',0.1);  %apply gamma adjustment for visibility and rescale to uint8 for gif
    end
    fprintf('\nreading back rendered image\n')
    [gifImage, ~]=imread(filename, 'frames', 'all');
    fprintf('displaying registration\n');
    f1=implay(gifImage);
    set(findall(0,'tag','spcui_scope_framework'),'position',[5 5 xSize ySize]);
    f1.DataSource.Controls.Repeat = 1;
    play(f1.DataSource.Controls);
end