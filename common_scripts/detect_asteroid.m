function [x1, y1, x2, y2] = detect_asteroid(lights_r)
%% auto asteroid detection based on movement detection in logical images
% input: lights_r, a registered set of lights that has one moving target
% output: x1, y1: coordinates of the asteroid in the first image of the series
%         x2, y2: coordinates of the asteroid in the last image
% Author: Jona Gladines
% Date: June - 2019
[ySize, xSize] = size(lights_r(:,:, 1));
[~,~,noi] = size(lights_r);
lights_sum = logical(zeros(ySize, xSize));
lights_bw = zeros(ySize,xSize,noi); 

for i=1:noi
    img = lights_r(:,:,i);
    sub = img(round((ySize/2)-(ySize/4)):round((ySize/2)+(ySize/4)), round((xSize/2)-(xSize/4)):round((xSize/2)+(xSize/4)));
    img2 = mat2gray(imadjust(uint16(img), [mean(nonzeros(min(sub)))/(2^16), mean(max(img))/(2^16)]));
    BW_in = logical(imbinarize(img2, 0.3));
    lights_bw(:,:,i) = bwpropfilt(BW_in, 'Eccentricity', [-Inf, 0.75 - eps(0.75)]);
    imshow(lights_bw(:,:,i), []);
    lights_sum = lights_sum | lights_bw(:,:,i);
end

%figure;
%imshow(lights_sum, []);
 
BW_out = bwpropfilt(lights_sum, 'Area', [20, Inf]);



%figure;
%imshow(BW_out, []);
 
BW_out = bwpropfilt(BW_out, 'Eccentricity', [0.8 + eps(0.8), Inf]);

BW_first = lights_bw(:,:,1) & BW_out;
s = regionprops(BW_first,'centroid');
xy_first = cat(1, s.Centroid);
x1 = xy_first(1);
y1 = xy_first(2);

BW_last = lights_bw(:,:,noi) & BW_out;
s = regionprops(BW_last,'centroid');
xy_last = cat(1, s.Centroid);
x2 = xy_last(1);
y2 = xy_last(2);
end