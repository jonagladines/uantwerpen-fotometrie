function [im0, ret_R0, ret_t0, out_angle] = register_images(images, registration_points)
% Original Code from astrotnstack
% Source: https://github.com/farhi/astrotnstack
% Modified by Jona Gladines for improved non integer image alignment.
% Date: June - 2019
% Dependencies: gauss2fit

% init options
options.tol_trans = 0.01; % tolerance on translation 1% of total image width
options.tol_rot   = 3;    % tolerance on rotation in degrees
options.N         = 10;   % number of control points 'stars'
options.images    = '';
options.silent    = 1;    % option to show registering at work
options.lim_in    = [];   % image adjustment parameters
options.lim_out   = [];   % image adjustment parameters
options.gamma     = 0.6;  % image adjustment parameters
f1 = '';

% pass 'options' into names variables
if exist("registration_points")
    N = registration_points;
else
    N        =options.N; 
end

tol_trans=options.tol_trans; 
tol_rot  =options.tol_rot;
%lim_in   =options.lim_in;
%lim_out  =options.lim_out;
%gamma    =options.gamma;


%% initiate treatment -----------------------------------------------------------
s = size(images, 3);
disp([ mfilename ': will treat ' num2str(s) ' files.' ]);

points.x = [];  % location  of 'stars', size [ numel(s) N ]
points.y = [];  % location  of 'stars', size [ numel(s) N ]
points.m = [];  % intensity of 'stars', size [ numel(s) N ]

if ~options.silent
  f1  = figure('Name','Current image');
  delete(findall(0, 'Tag', [ mfilename '_waitbar' ]));
  wb  = waitbar(0, 'registering images...'); set(wb, 'Tag', [ mfilename '_waitbar' ]);
end

im0 = zeros(size(images));
out_angle = zeros(size(image, 3));
ret_R0 = zeros(2, 2, size(images, 3));
ret_t0 = zeros(2, 1, size(images, 3));
t0 = clock;
totalimages    = 0;

%% start registration and alignement
for index=1:s

  if ~options.silent
    if ~ishandle(wb)
      disp('Aborting.')
      break;
    end
    try
      waitbar(index/s, wb, s);
    catch
        disp('problem with waitbar, exiting');
        break;
    end
  end
  
  name = sprintf('image_%i', index);
  im = images(:,:,index);
%   if ~isinteger(im) && isreal(im) || ~isa(im, 'uint8')
%     im = uint8(double(im)*256/max(double(im(:))));  % make it a uint8
%   end
  totalimages = totalimages + 1;
  
  if totalimages == 1 % reference
    disp([ mfilename ': ' num2str(index) '/' num2str(s) ': ' name ]);
    im0(:,:,1)  = uint16(im);
  else
    if totalimages > 2
      dt_from0     = etime(clock, t0);
      dt_per_image = dt_from0/(index-2);
      % remaining images: s-index
      eta    = dt_per_image*(s-index+1);
      ending = addtodate(now, ceil(eta), 'second');
      ending = [ 'Ending ' datestr(ending) ];
      eta    = sprintf('ETA %i [s]. %s', round(eta), ending);
    else
        eta='';
        ending='';
    end
    disp([ mfilename ': ' num2str(index) '/' num2str(s) ': ' name '. ' eta]);
    if ~options.silent
      if ishandle(wb)
        set(wb, 'Name', [ num2str(index) '/' num2str(s) ' ' ending ])
      else
          break;
      end
    end
    %meta = display_metainfo(s{index}, false);
    %meta = '';
    %if isempty(meta), continue; end
  end

  %% find control points on gray image
  %points = find_control_points(imadjust(uint16(im), lim_in, lim_out, gamma), totalimages, N, points, tol_trans*size(im,1));
  points = findstars(im, totalimages, N, points, tol_trans*2*size(im,1));

   if totalimages == 1
     continue; 
   end

%    if index == 7
%        disp("hello")
%    end
  
  % now we identify the points which keep distances and angles continuous
  % from one image to the next

  [x1,y1,x2,y2, p1_orig,p2_orig, p1_axis,p2_axis] = analyse_dist_angles(...
    points, totalimages, tol_trans*size(im,1), tol_rot);

  % get the best similarity
  if ~p1_orig || ~p2_orig || ~p1_axis || ~p2_axis
    disp([ mfilename ': WARNING: not enough control points for ' num2str(index) ' (axis). Will ignore image ' name ])
    im0(:,:,index) = uint16(im);
    continue;
  end
  
  % identify the current points which are common to the reference, and match
  % distances AND rotations
  p1 = p1_orig; p2=p2_orig;
  d1  = sqrt( (x1-x1(p1)).^2 + (y1-y1(p1)).^2 );
  t1  = atan2( y1-y1(p1),       x1-x1(p1))*180/pi;
  d2  = sqrt( (x2-x2(p2)).^2 + (y2-y2(p2)).^2 );
  t2  = atan2( y2-y2(p2),       x2-x2(p2))*180/pi;
  [ok1,ok2] = find_similar2(d1,             d2,             tol_trans*size(im,1), ...
                            t1-t1(p1_axis), t2-t2(p2_axis), tol_rot*pi/180);
  
  % plot image
  if ~options.silent && ishandle(f1)
    figure(f1);
    [h,colors]=plot_im_points(im, name, ...
      points.x(totalimages,:), points.y(totalimages,:), points.m(totalimages,:));
    if all(ishandle(h))
      % we highlight p2_orig and p2_axis control points
      set(h(p2_orig),'MarkerFaceColor',colors(p2_orig));
      set(h(p2_axis),'MarkerFaceColor',colors(p2_axis));
      drawnow
    end
  end
  
  if numel(ok1) <= 1 || numel(ok2) <= 1
    disp([ mfilename ': WARNING: not enough control points for ' num2str(index) ' (common). Will ignore image ' name ])
        im0(:,:,index) = uint16(im);
    continue
  end
  
  % we make a check for wrong guesses
  delta = (t1(ok1)-t1(p1_axis)) - (t2(ok2)-t2(p2_axis));
  bad   = find(abs(delta) > tol_rot*3);
  ok1(bad) = [];
  ok2(bad) = [];
  if numel(ok1) <= 1 || numel(ok2) <= 1
    disp([ mfilename ': WARNING: not enough control points for ' num2str(index) ' (common2). Will ignore image ' name ])
    im0(:,:,index) = uint16(im);
    continue
  end
  
  % compute the mean translation (x,y) and rotation angle wrt to reference image
  x1  = x1(ok1); y1=y1(ok1);
  x2  = x2(ok2); y2=y2(ok2);
  
  % theta2-theta1 is an estimate of the rotation angle
  % compute the affine transformatin
  [ret_R, ret_t] = rigid_transform_3D([x2 ; y2]', [x1 ; y1]');
  if isempty(ret_R)
    disp([ mfilename ': WARNING: invalid affine transformation. Skipping.'])
    continue
  end

  % compute an estimate of the translation and rotation from the identified
  % control points orig and axis
  theta1 = t1(p1_axis); % t1(p1_orig) == 0
  theta2 = t2(p2_axis);
  theta = asind(ret_R(1,2));
  if abs(theta-(theta2-theta1)) > tol_rot
    disp([ mfilename ': WARNING: invalid affine rotation. Skipping.']);
    continue
  end
  disp([ '  Angle=' num2str(theta,3) ' [deg] ; Translation=' mat2str(ret_t,3) ' [pix]']);
  
  % apply the rotation and stack raw counts
  [im,~] = imaffine(im, ret_R, ret_t);
  im0(:,:,index) = uint16(im);
  out_angle(index) = theta;
  ret_R0(:, :, index) = ret_R;
  ret_t0(:, :, index) = ret_t;
  clear im
end % for index

if  ~options.silent && ishandle(f1), close(f1); end
if  ~options.silent && ishandle(wb), close(wb); end
disp([ mfilename ': Elapsed time ' num2str(etime(clock, t0)) ' [s]' ])

end