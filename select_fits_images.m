function [images, dates, shutter] = select_fits_images(varargin)
% Function to select a single or multiple fits images with a file selector.
% output: in case of a single image selection, that image its creation date
% (creation date is start of exposure) and the length of the exposure
% in case of multiple images it returns an array with these images, their
% creation dates and exposure times in arrays
% Written by Jona Gladines
% For Uantwerpen
% Date June - 2019

    if nargin == 1
        filetype = varargin{1};
    else
        filetype = 'Images';
    end
    disp(['Select ' filetype]);
    [lfiles,ldir]=uigetfile({'*.fit'},['Select ' filetype],'Multiselect','on');
    if iscell(lfiles) 
        noi=numel(lfiles);                                                     %number of images
        
        filename=strcat(ldir, lfiles(1));
        [ySize, xSize] = size(fitsread(filename{1}));

    % Read images
        images = zeros(ySize,xSize,noi);                                                                        %preallocate array for lights
        dates = linspace(datetime('today'), datetime('today')+seconds(noi), noi);                               %preallocate dates
        for i = 1:noi
            filename=strcat(ldir, lfiles(i));
            images(:,:,i) = fitsread(filename{1});                                                               %read fits file
            info = fitsinfo(filename{1});                                                                       % read fits info for date
            date_obs = [info.PrimaryData.Keywords{strcmp(info.PrimaryData.Keywords(:,1),'DATE-OBS'),2}];        %find the date string in the fits info
            shutter = [info.PrimaryData.Keywords{strcmp(info.PrimaryData.Keywords(:,1),'EXPTIME'),2}];
            datestring = date_obs(1:19);                                                                        
            dates(i) = datetime(datestring,'InputFormat','yyyy-MM-dd''T''HH:mm:ss');                            %parse date string into date time object
        end
    else
        if ischar(lfiles)
            filename=strcat(ldir, lfiles);
            images = fitsread(filename);
            info = fitsinfo(filename);
            date_obs = [info.PrimaryData.Keywords{strcmp(info.PrimaryData.Keywords(:,1),'DATE-OBS'),2}];        %find the date string in the fits info
            shutter = [info.PrimaryData.Keywords{strcmp(info.PrimaryData.Keywords(:,1),'EXPTIME'),2}];
            datestring = date_obs(1:19);                                                                        
            dates = datetime(datestring,'InputFormat','yyyy-MM-dd''T''HH:mm:ss');                            %parse date string into date time object
            warning('You only selected one image');
        else
            warning('Operation Canceled, no images selected.');
            images = 0;
            dates = 0;
            shutter = 0;
        end
    end
end

