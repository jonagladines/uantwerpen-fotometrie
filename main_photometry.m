%   UAntwerpen Physics Department Photometry Project
% 
%   Load, Register and Process images taken with a telescope
%
%   Written by Jona Gladines
%   E-MAIL: jona.gladines@uantwerpen.be
%   Date: 06/01/2019
%   Update: April - 2022

% Dependencies: image toolbox, curve fit toolbox
% Required scripts: aper.m, box_cursor.m, register_images.m, gauss2fit.m,
% select_fits_images.m, show_registration.m, select_objects.m, findstars.m


%% Configurable Parameters
close all;

registration_points= 20; %number of stars to use for image registration.
box_size=15;            %size of searchbox around selected point for detecting asteroid/variable star location.

app=7;                  %apperture for photometry
ir=12;                  %inner radius for sky annulus
or=25;                  %outer radius for sky annulus

nors = 6;               %number of reference stars
mag = [12.94, 12.98, 12.40, 12.27, 13.28, 13.75];               %magnitudes of reference stars. 
do_fft = false;         %execute the FFT algorithm to find the periods

%% Data import and structuring
%ask for object name
name = inputdlg('What is the name of the object?');

% read files
[darks, ~, ~] = select_fits_images('Darks');
if size(darks, 3) > 1                       %in case only one file is selected, the single image is treated as master dark
    masterdark = median(darks, 3);
else
    masterdark = darks;
end
% 
% [flatdarks, ~, ~] = select_fits_images('Flatdark');  %in case only one file is selected, the single image is treated as master flatdark
% if size(flatdarks, 3)>1
%   masterflatdark = median(flatdarks, 3);
% else
%     masterflatdark = flatdarks;
% end

[flats, ~, ~] = select_fits_images('Flats');       %in case only one file is selected, the single image is treated as master flat
if size(flats, 3) > 1
    %masterflat = median(flats - masterflatdark, 3);
    masterflat = median(flats, 3);
else
    masterflat = flats - masterflatdark;
end



%% Data Calibration
[lights, dates, ~] = select_fits_images('Lights');
[~,~,noi] = size(lights);
[ySize, xSize] = size(lights(:,:,1));

% correct lights with darks, flats,...
lights_c = ((lights - masterdark)./(masterflat)) * mean(masterflat(:));

% register images
[lights_r, rotation, translation, angle] = register_images(lights_c, registration_points);
%display registration result
show_registration(lights_r,  'registration.gif');

%% Target and reference Selection
%select target in first image
[y0, x0] = select_objects(lights_r(:,:,1), box_size, 1, 'Select Target');

% select target in last image
[y1, x1] = select_objects(lights_r(:,:,noi), box_size, 1, 'Select Target');

% calculating x and y speed of object
dx=x1-x0;                               %number of pixels traveled in x dir
dy=y1-y0;                               %number of pixels traveled in y dir
dt=seconds(dates(noi)-dates(1));        %time in seconds between first and last image
vx=dx/dt;                               %x speed in pixels / second
vy=dy/dt;                               %y speed in pixels / second

%select reference stars
[rst_y, rst_x, mag] = select_objects(lights_r(:,:,1), box_size, nors, 'Select Reference Stars');
ref_stars = [rst_y, rst_x];

%% Processing

[p_corr, data] = photometry(lights_r, dates, x0, y0, vx, vy, ref_stars, app, ir, or, mag);      %2.3 is ccd specific saturation

for i = 1:nors
    a = (p_corr(i, 1:end-2)+p_corr(i, 3:end))/2;
    b = p_corr(i, 2:end-1);
    noise = b-a;
    rms_noise(i) = rms(noise)*sqrt(2/3);           %calculate the rms noise of the final averaged curve based on neighbouring data points
end

if nors > 1
    p_mean = mean(p_corr);                                          %calculate mean of all traces if there is more than one trace
else
    p_mean = p_corr;
end

a = (p_mean(1:end-2)+p_mean(3:end))/2;
b = p_mean(2:end-1);
noise = b-a;
rms_noise_mean = rms(noise)*sqrt(2/3);           %calculate the rms noise of the final averaged curve based on neighbouring data points


%% Plot result of measurements
disp('generating plot');
f6=figure('Name', 'Measured Data','NumberTitle','off');
movegui(f6, 'center');
plot(dates, p_corr, '.');                                           %plot individual data traces
set(gca, 'YDir','reverse');
xlabel('time (UTC)')
ylabel('measured magnitude');
title([name " vs. reference stars"])


if nors > 1
    for i = 1:nors
        legends{i} = sprintf('%s vs ref star %d', name{1}, i);
    end
    legend(legends);
else
    legend(sprintf('%s vs ref star', name{1}));
end

f7 = figure();
plot(dates, p_mean, 'x');
set(gca, 'YDir','reverse');
xlabel('time (UTC)')
ylabel('measured magnitude');
title(name{1});
    
%% find frequency components through fft
if do_fft == true
    G = findgroups(dates);                                                      % Unique Time Values
    p_meanu = splitapply(@mean, p_mean, G);                                     % Take Mean Of Values For Each �date� Element
    N = numel(p_meanu);                                                         % Number Of Samples
    datesv = linspace(min(dates), max(dates), N);                               % Create Uniformly-Sampled Vector For Resampling Interpolation
    Ts = datenum(mean(diff(datesv)));                                           % Sampling Interval (Days)
    Fs = 1/Ts;                                                                  % Sampling Frequency (Samples/Day)
    Fn = Fs/2;                                                                  % Nyquist Frequency (Samples/Day)
    p_meanur = resample(p_meanu, datesv);                                       % Resample To Uniformly-Sampled Time Vector
    NFFT = 2*nextpow2(N);                                                       % Length Of Fourier Transform
    p_meanurm = p_meanur - mean(p_meanur);                                      % Subtreact Mean
    FTp_meanur = fft(p_meanurm, 2^NFFT)/N;                                      % Fourier Transform
    Fv = linspace(0, 1, fix(numel(FTp_meanur)/2)+1)*Fn;                         % Frequency Vector
    Iv = 1:numel(Fv);                                                           % Correspoinding Index Vector
    [pks, loc] = findpeaks(abs(FTp_meanur(Iv)), 'MinPeakHeight',0.0075);       % Fourier Transform Peak Am,plitudes & Locations
    %[pks, loc] = max(abs(FTp_meanur(Iv)));                                       % find location of maximum peak
    pks_order = floor(log(abs(pks(1)))./log(10));
    
    
    f8 = figure('Name', 'FFT on resampled data','NumberTitle','off');
    %movegui(f8, 'east');
    plot(Fv, abs(FTp_meanur(Iv)))
    hold on
    plot(Fv(loc), pks, '^r', 'MarkerFaceColor','r')
    grid
    period = duration(days(1./Fv(loc)), 'format', 'hh:mm:ss');
    %pklblc = sprintf('Rel. Magnitude = %6.4f\nPeriod (hh:mm:ss) = %s', pks, char(period));
    %text(Fv(loc), pks, pklblc, 'HorizontalAlignment','left','VerticalAlignment','bottom');
    pklblc = compose('Amp = %6.4f\nPerd = %6.4f', [pks; 1./Fv(loc)]');
    text(Fv(loc), pks, pklblc, 'HorizontalAlignment','left', 'VerticalAlignment','bottom')
    
    ylim([0  ceil((max(pks)*(10^(pks_order*-1))))/(10^(pks_order*-1))])
    xlabel('Frequency (Periods/Day)');
    ylabel('Magnitude Variation');
end

