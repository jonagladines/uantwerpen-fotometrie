function [h,colors]=plot_im_points(im, t, x,y, m)
% plot_im_points: plot an image and the 'max' star locations
%
% input:
%   im:  image
%   t:   title (filename)
%   x,y: locations (vectors)
%   m:   intensity (vector)
  h = [];
  imshow(im, []); title(t); hold on;
  colors =repmat('rgbmcy',1, numel(x));
  for p=1:numel(x)
    if ~x(p) || ~y(p) || m(p) <= 0 || ~isfinite(m(p)), continue; end
    h1=plot(y(p),x(p),colors(p));
    set(h1,'Marker','o', 'MarkerSize',m(p)/1024);
    h = [ h h1 ];
  end
  hold off
  drawnow
  set(gcf, 'Name', t);
end