function [r, c, mag] = select_objects(img, box_size, quantity, varargin)
    % img = image to select objects in
    % box_size = size of bounding box surrounding the selected point in which a the object must be found
    % noo = number of objects
    % output: r = row or y coordinate of objects
    %         c = column or x coordinate of objects
    % Depenedencies: box_cursor.m
    % Author: Jona Gladines
    % Date: June - 2019
    % Update April - 2022

    if nargin == 4
        title = varargin{1};
    else
        title = 'Select Object';
    end
    
    f=figure('Name', title,'NumberTitle','off');
    [ySize, xSize] = size(img);
    sub = img(round((ySize/2)-(ySize/4)):round((ySize/2)+(ySize/4)), round((xSize/2)-(xSize/4)):round((xSize/2)+(xSize/4)));            %cutout center of image to remove artefacts from registering
    imshow(imadjust(uint16(img), [(mean(nonzeros(img))-3*std(nonzeros(img)))/65535, (mean(nonzeros(img))+6*std(nonzeros(img)))/65535]));                %display 1st image for object selection
    colormap gray;
    hold on;
    r = zeros(quantity, 1);
    c = zeros(quantity, 1);
    for i=1:quantity
        [x0,y0,~,~] = box_cursor;                       %enable selection of object
        sub=img(y0-((box_size-1)/2):y0+((box_size-1)/2), x0-((box_size-1)/2):x0+((box_size-1)/2));  %take submatrix of box_size out of image

        
        sub2=mat2gray(sub, [min(sub(:)) max(sub(:))]);
        %figure;
        %imshow(sub2, []);
        sub_bw = logical(imbinarize(sub2, 0.2));
        %figure;
        %imshow(sub_bw, []);
        s = regionprops(sub_bw,'centroid');
        xy = cat(1, s.Centroid);

        R = xy(2);
        C = xy(1);
        [~,I]=max(sub(:));                              %find maximum of submatrix and its index
        [R1,C1]=ind2sub(size(sub),I);                     %convert the index to rows and columns
        r(i)=y0-((box_size-1)/2)+R-1;                     %recalculate X and Y coordinates of object based on brightest point in submatrix
        c(i)=x0-((box_size-1)/2)+C-1;
        plot(c(i), r(i),'or','markersize',12,'linewidth',1.5);
        if quantity>1
            mag(i) = str2double(inputdlg('What is the magnitude of the reference star?'));
        end
    end
    hold off;
end