# UAntwerpen Astrometrie

Collection of functions to be used for astrometry measurements of Uantwerpen Students

main_photometry.m: main script to process raw data, includes references to the other functions

[images, dates, shutter] = select_fits_images(): Opens a file dialog box to select a single or multiple fits files.
	in case one file is selected, only one file is returned, otherwise an array of images is returned
	
[lights_r, rotation, translation] = register_images(lights): registers all images to the first image in the sequence 
	and carries out an affine transformation. it returns the registered images and the transformation parameters

show_registration(lights_r, filename): creates an animated gif with 'filename' of the registered image sequence, carries out a simple stretch 
	to balance out the background accross all images. the resulting gif is displayed with the video player.

[y, x] = select_objects(image, box_size, quantity): displays the stretched image and allows the user to select a number of objects.
	The number is defined by 'quantity'. the function will look for the object in the box defined by box_size, calculate the centroid
	and return the floating point location of the object.

p_corr = photometry(lights_r, dates, x0, y0, vx, vy, ref_stars, app, ir, or, mag): will calculate the magnitudes of the target and the 
	reference stars based on the measured flux and the reference magnitude 'mag'. The flux is measured using aperture photometry. 
	app is the aperture diameter to measure the flux, ir and or are the inner and outer radius for the region arround the apperture circle
	which is used to subtract the background, called sky annulus. x0 and y0 are the coordinates for the target on the first image, vx and vy
	are the x and y speed in pixels/s of the target (mainly used for asteroids). reference stars is an 2 by ... array of x and y coordinates
	for the chosen refrence stars.
